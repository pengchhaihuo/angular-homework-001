import {Component, OnChanges, OnInit} from '@angular/core';

@Component({
  selector: 'app-light',
  templateUrl: './light.component.html',
  styleUrls: ['./light.component.css']
})
export class LightComponent implements OnInit {

  imgOnUrl = "/assets/on.png"
  imgOffUrl = "/assets/off.png"

  imgUrl!:String
  userText!:String

  customColor:String = this.userText

  constructor() { }

  ngOnInit(): void {
    this.imgUrl = this.imgOffUrl
  }




  //------------ on/off lamp function -----------------
  toggleLight(){
    if(this.imgUrl.localeCompare(this.imgOffUrl)===0){
      console.log("on")
      this.imgUrl=this.imgOnUrl
    }else {
      console.log("off")
      this.imgUrl=this.imgOffUrl
    }
  }

}
